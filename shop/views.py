from rest_framework import viewsets, mixins, generics
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response

from shop.utils import transfer_item
from .serializers import SellOrderSerializer, BuyOrderSerializer
from .models import SellOrder, BuyOrder
from Itemist.permissions import IsOwnerOrAdmin
from trade.trade_utils import send_item


class SellOrderViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet
):
    """
    endpoint for get/ SellOrder items
    """
    queryset = SellOrder.objects.all()
    serializer_class = SellOrderSerializer

    def get_permissions(self):
        if self.action in ['destroy', 'partial_update']:
            permission_classes = [IsOwnerOrAdmin]
        elif self.action == 'create':
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [AllowAny]
        return [permission() for permission in permission_classes]

    def list(self, request, *args, **kwargs):
        filters = self.get_list_filter_parameters()
        queryset = SellOrder.objects.filter(**filters).all()
        serializer = SellOrderSerializer(queryset, many=True)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        sell_order = SellOrder.objects.get(pk=pk)
        serializer = self.serializer_class(sell_order, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    def get_list_filter_parameters(self):
        filters = {}
        allowed_fields = ["steam_id", "sold"]
        for field_filter, value in self.kwargs.items():
            if field_filter in allowed_fields:
                filters[field_filter] = value
        return filters


class BuyOrderViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = BuyOrderSerializer
    queryset = BuyOrder.objects.all()

    def create(self, request, *args, **kwargs):
        sell_order_ids = request.data["sell_order_pks"]
        sell_orders = list()
        for sell_order_id in sell_order_ids:
            sell_order = SellOrder.objects.get(pk=sell_order_id)
            sell_orders.append(sell_order)
            total_price = sell_order.price
            game_id = sell_order.item.app_id
        buyer = request.user
        if self.request.wallet.balance > total_price:
            wallet_transaction = buyer.wallet.withdraw(
                total_price, f"bought orders:{sell_orders}"
            )
            buy_orders = list()
            for sell_order in sell_orders:
                new_buy_order = BuyOrder.objects.create(
                    buyer=buyer,
                    sell_order=sell_order,
                    item=sell_order.item,
                    price=sell_order.price,
                    transaction=wallet_transaction,
                )
                sell_order.sold = True
                sell_order.save(update_fields=['sold'])
                buy_orders.append(new_buy_order)
            trade_offer = send_item(
                receiver=buyer, game_id=game_id, item_asset_ids=[buy_order.item.asset_id for buy_order in buy_orders]
            )
            for buy_order in buy_orders:
                buy_order.trade_offer = trade_offer
                buy_order.save(update_fields=['trade_offer'])
                transfer_item(buy_order.item, buyer)

            return Response(buy_orders)
        else:
            return Response("not enough money")

    def filter_queryset(self, queryset):
        return self.get_queryset().filter(buyer=self.request.user)
