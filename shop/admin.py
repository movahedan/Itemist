from django.contrib import admin

from .models import SellOrder, BuyOrder

admin.site.register(SellOrder)
admin.site.register(BuyOrder)
