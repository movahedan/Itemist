from django.db import models

from accounts.models import SteamUser, WalletTransaction
from items.models import Item
from trade.models import Trade


class SellOrder(models.Model):
    sold = models.BooleanField(default=0)
    seller = models.ForeignKey(SteamUser, on_delete=models.CASCADE)
    transaction = models.ForeignKey(
        WalletTransaction,
        on_delete=models.SET_DEFAULT,
        default=None,
        null=True,
        blank=True,
    )
    trade_offer = models.ForeignKey(Trade, on_delete=models.SET_NULL, null=True, blank=True)
    item = models.OneToOneField(Item, on_delete=models.CASCADE)
    price = models.FloatField()
    promoted = models.BooleanField(default=0)
    date_created = models.DateTimeField(auto_now_add=True)


class BuyOrder(models.Model):
    success = models.BooleanField(default=0)
    sell_order = models.ForeignKey(SellOrder, on_delete=models.CASCADE)
    buyer = models.ForeignKey(
        "accounts.SteamUser", on_delete=models.SET_NULL, null=True, blank=True
    )
    transaction = models.ForeignKey(
        WalletTransaction,
        on_delete=models.SET_DEFAULT,
        default=None,
        null=True,
        blank=True,
    )
    trade_offer = models.ForeignKey(Trade, on_delete=models.SET_NULL, null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True)
