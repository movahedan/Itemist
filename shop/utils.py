from accounts.models import SteamUser
from items.models import Item


def transfer_item(item: Item, to_person: SteamUser) -> None:
    item.owner = to_person
    item.save(update_fields=["owner"])
