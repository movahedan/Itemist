from rest_framework import routers

from .views import SellOrderViewSet, BuyOrderViewSet

router = routers.DefaultRouter()
router.register(r"sell", SellOrderViewSet, base_name="sellorder")
router.register(r"buy", BuyOrderViewSet, base_name="buy")

app_name = "shop"
urlpatterns = [] + router.urls
