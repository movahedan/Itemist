from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .models import SellOrder, BuyOrder
from items.serializers import Dota2ItemsSerializer, ItemSerializer


class SellOrderSerializer(serializers.ModelSerializer):
    item = ItemSerializer()

    class Meta:
        model = SellOrder
        fields = ("item", 'sold', 'seller', "price", "date_created", "promoted")
        read_only_fields = ('sold', 'seller', 'date_created', 'promoted')

    def create(self, validated_data):
        if validated_data["item"].owner == self.context["request"].user:
            seller = self.context["request"].user
            return SellOrder.objects.create(**validated_data, seller=seller)
            # trade_offer = receive_item(
            #     sender=seller, game_id=game_id, sender_item_asset_ids=[item.asset_id]
            # )
            # new_sell_order.trade_offer = trade_offer
            # new_sell_order.save(update_fields=["trade_offer"])
            # todo: un tested code
        else:
            raise ValidationError("you are not owner of this item!")

    def update(self, instance, validated_data):
        validated_data.pop('item')
        SellOrder.objects.filter(id=instance.pk).update(**validated_data)
        instance.refresh_from_db()
        return instance


class BuyOrderSerializer(serializers.ModelSerializer):
    sell_order = SellOrderSerializer()

    class Meta:
        model = BuyOrder
        fields = ('sell_order', 'success', 'date_created')
