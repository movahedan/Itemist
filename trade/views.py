from django.db.models import Q

from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticated

from Itemist.permissions import IsOwnerOrAdmin
from .models import Trade
from .serializers import TradeSerializer


class TradeList(mixins.RetrieveModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = (IsOwnerOrAdmin, IsAuthenticated)
    serializer_class = TradeSerializer
    queryset = Trade.objects.all()

    def filter_queryset(self, queryset):
        return queryset.filter((Q(sender=self.request.user) | Q(receiver=self.request.user))).all()
