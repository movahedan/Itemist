from rest_framework import serializers

from .models import Trade


class TradeSerializer(serializers.ModelSerializer):
    on_which_side_of_trade = serializers.SerializerMethodField(source="get_on_which_side_of_trade")

    class Meta:
        model = Trade
        exclude = ("sender", "receiver")

    def get_on_which_side_of_trade(self, obj):
        if self.context["request"].user == obj.sender:
            return "sender"
        elif self.context["request"].user == obj.receiver:
            return "receiver"
