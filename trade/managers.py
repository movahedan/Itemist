from django.db import models


class TradeManager(models.Manager):
    def create_send_item_offer(self, trade_data):
        trade = self.model(
            trade_offer_id=trade_data["tradeofferid"],
            trade_offer_state="PENDING",
            receiver=trade_data["receiver"],
        )
        trade.save()
        for item in trade_data["sender_items"]:
            trade.sender_items.add(item)
        trade.save(update_fields=["sender_items"])
        return trade

    def create_receive_item_offer(self, trade_data):
        trade = self.model(
            trade_offer_id=trade_data["tradeofferid"],
            trade_offer_state="PENDING",
            sender=trade_data["sender"],
        )
        trade.save()
        for item in trade_data["receiver_items"]:
            trade.receiver_items.add(item)
        trade.save(update_fields=["receiver_items"])
        return trade
