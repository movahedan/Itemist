from rest_framework.routers import DefaultRouter

from .views import TradeList

router = DefaultRouter()
router.register(r'trades', TradeList, 'trades')

app_name = "trade"
urlpatterns = [] + router.urls
