import sys

from django.conf import settings
from steampy.client import Asset
from steampy.models import GameOptions

from accounts.models import SteamUser
from loguru import logger

from items.models import Item
from trade.models import Trade

logger.add(
    sys.stderr, format="{time} {level} {message}", filter="trade_offers", level="INFO"
)

steam_client = getattr(settings, "steam_client")


def receive_item(sender: SteamUser, game_id: int, sender_item_asset_ids: list):
    logger.log(
        f"creating offer to steam id: {sender.steam_id} to get item:{sender_item_asset_ids} in game:{game_id}"
    )
    game_name = get_game_object(game_id)
    assets = list()
    items = list()
    for item_asset_id in sender_item_asset_ids:
        assets.append(Asset(item_asset_id, game=game_name))
        items.append(Item.objects.get(asset_id=item_asset_id, game_id=game_id))
    if getattr(settings, "TEST_WITHOUT_BOT"):
        trade_offer_data = steam_client.make_offer_with_url(
            items_from_me=[],
            items_from_them=assets,
            trade_offer_url=sender.trade_url,
            message="this offer has been sent by Itemist website",
        )
    else:
        trade_offer_data = {"tradeofferid": 0}
    logger.log(
        f"sent offer to steam id: {sender.steam_id} to get item:{items} in game:{game_id}"
    )
    trade_offer_data["sender_items"] = items
    trade_offer_data["sender"] = sender
    trade_offer = Trade.objects.create_receive_item_offer(trade_offer_data)
    return trade_offer


def send_item(receiver: SteamUser, game_id: int, item_asset_ids: list):
    logger.log(
        f"creating offer to steam id: {receiver.steam_id} to send item:{item_asset_ids} in game:{game_id}"
    )
    game = get_game_object(game_id)
    assets = list()
    items = list()
    for item_asset_id in item_asset_ids:
        assets.append(Asset(item_asset_id, game=game))
        items.append(Item.objects.get(asset_id=item_asset_id, game_id=game_id))
    if getattr(settings, "TEST_WITHOUT_BOT"):
        trade_offer_data = steam_client.make_offer_with_url(
            items_from_me=assets,
            items_from_them=[],
            trade_offer_url=receiver.trade_url,
            message="this offer has been sent by Itemist website",
        )
    else:
        trade_offer_data = {"tradeofferid": 0}
    logger.log(
        logger.log(
            f"sent offer to steam id: {receiver.steam_id} to get item:{item_asset_ids} in game:{game_id}"
        )
    )
    trade_offer_data["receiver_items"] = items
    trade_offer_data["receiver"] = receiver
    trade_offer = Trade.objects.create_send_item_offer(trade_offer_data)
    return trade_offer


def get_game_object(game_id: int) -> GameOptions:
    game_objects = {570: GameOptions.DOTA2}
    try:
        return game_objects[game_id]
    except KeyError:
        logger.log(f"cannot find game_id: {game_id}")
        raise Exception(f"game_id: {game_id} is not defined")
