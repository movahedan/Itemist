from django.db import models

from accounts.models import SteamUser
from items.models import Item
from trade.managers import TradeManager

from .config import TRADE_OFFER_STATE_CHOICES


class Trade(models.Model):
    trade_offer_id = models.IntegerField()
    trade_offer_state = models.CharField(choices=TRADE_OFFER_STATE_CHOICES, max_length=20)
    sender = models.ForeignKey(
        SteamUser, on_delete=models.CASCADE, related_name="trade_receiver"
    )
    receiver = models.ForeignKey(
        SteamUser, on_delete=models.CASCADE, related_name="trade_sender"
    )
    sender_items = models.ManyToManyField(
        Item, related_name="sender_items"
    )
    receiver_items = models.ManyToManyField(
        Item, related_name="receiver_items"
    )

    objects = TradeManager()
