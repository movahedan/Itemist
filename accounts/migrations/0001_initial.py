# Generated by Django 2.2 on 2019-04-26 14:06

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ('auth', '0011_update_proxy_permissions'),
    ]

    operations = [
        migrations.CreateModel(
            name='SteamUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False,
                                                     help_text='Designates that this user has all permissions without explicitly assigning them.',
                                                     verbose_name='superuser status')),
                ('steam_id', models.CharField(max_length=17, unique=True)),
                ('person_name', models.CharField(blank=True, max_length=255, null=True)),
                ('first_name', models.CharField(blank=True, max_length=30, null=True)),
                ('last_name', models.CharField(blank=True, max_length=30, null=True)),
                ('profile_url', models.CharField(blank=True, default='', max_length=300, null=True)),
                ('trade_url', models.CharField(blank=True, max_length=300, null=True)),
                ('phone_number', models.CharField(blank=True, max_length=17, validators=[
                    django.core.validators.RegexValidator(message='شماره را به صورت +98123456789 وارد کنید',
                                                          regex='^\\+?98?\\d{10,10}$')])),
                ('email', models.EmailField(max_length=254)),
                ('avatar', models.CharField(blank=True, max_length=255, null=True)),
                ('avatar_medium', models.CharField(blank=True, max_length=255, null=True)),
                ('avatar_full', models.CharField(blank=True, max_length=255, null=True)),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('is_active', models.BooleanField(default=True)),
                ('is_staff', models.BooleanField(default=False)),
                ('groups', models.ManyToManyField(blank=True,
                                                  help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.',
                                                  related_name='user_set', related_query_name='user', to='auth.Group',
                                                  verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.',
                                                            related_name='user_set', related_query_name='user',
                                                            to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Wallet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('balance', models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=64, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('owner',
                 models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'wallet',
                'verbose_name_plural': 'wallets',
            },
        ),
        migrations.CreateModel(
            name='WalletTransaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=64, null=True)),
                ('running_balance',
                 models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=64, null=True)),
                ('kind', models.CharField(max_length=20)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created_at')),
                ('wallet',
                 models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='accounts.Wallet')),
            ],
            options={
                'verbose_name': 'Wallet Transaction',
                'verbose_name_plural': 'Wallet Transactions',
            },
        ),
    ]
