from django.urls import path
from rest_framework import routers

from .views import LogoutView, LoginView, ProfileViewSet, WalletTransactionViewSet, WalletViewSet, BankAccountsViewSet

router = routers.DefaultRouter()
router.register(r"profile", ProfileViewSet, base_name="profile")
router.register(r"wallet", WalletViewSet, base_name="wallet")
router.register(
    r"wallet-transactions",
    WalletTransactionViewSet,
    base_name="wallet transactions",
)
router.register(r"bank-accounts", BankAccountsViewSet, base_name='bank-account')

app_name = "accounts"
urlpatterns = [
    path("logout/", LogoutView.as_view(), name="logout"),
    path("login/", LoginView.as_view(), name="login"),
              ] + router.urls
