from accounts.models import SteamUser


def get_username(strategy, uid, user=None, *args, **kwargs):
    """Removes unnecessary slugification and cleaning of the username since the uid is unique and well formed"""
    if not user:
        username = uid
    else:
        username = strategy.storage.user.get_username(user)
    return {"username": username}


def user_details(user, details, strategy, *args, **kwargs):
    """Update user details using data from provider."""
    if user:
        changed = False  # flag to track changes
        protected = ("steam_id", "id", "pk") + tuple(
            strategy.setting("PROTECTED_USER_FIELDS", [])
        )

        # Update user model attributes with the new data sent by the current
        # provider. Update on some attributes is disabled by default, for
        # example username and id fields. It's also possible to disable update
        # on fields defined in SOCIAL_AUTH_PROTECTED_FIELDS.
        if details["player"]:
            setattr(user, "person_name", details["player"]["personaname"])
            for name, value in details["player"].items():
                if value is not None and hasattr(user, convert_conversion(name)):
                    current_value = getattr(user, convert_conversion(name), None)
                    if not current_value or convert_conversion(name) not in protected:
                        changed |= current_value != value
                        setattr(user, convert_conversion(name), value)

        if changed:
            strategy.storage.user.changed(user)


def convert_conversion(variable: str) -> str:
    ref = {
        "steamid": "steam_id",
        "personaname": "person_name",
        "profileurl": "profile_url",
        "avatarmedium": "avatar_medium",
        "avatarfull": "avatar_full",
    }
    return ref.get(variable, variable)


def associate_existing_user(uid, *args, **kwargs):
    """If there already is an user with the given steam_id, hand it over to the pipeline"""
    if SteamUser.objects.filter(steam_id=uid).exists():
        return {"user": SteamUser.objects.get(steam_id=uid)}
