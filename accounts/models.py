from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.core.validators import RegexValidator
from django.conf import settings

from accounts.managers import SteamUserManager, WalletTransactionManager


class SteamUser(AbstractBaseUser, PermissionsMixin):
    USERNAME_FIELD = "steam_id"

    steam_id = models.CharField(max_length=17, unique=True)
    person_name = models.CharField(max_length=255, null=True, blank=True)
    first_name = models.CharField(max_length=30, null=True, blank=True)
    last_name = models.CharField(max_length=30, null=True, blank=True)
    profile_url = models.CharField(max_length=300, default="", null=True, blank=True)
    trade_url = models.CharField(max_length=300, null=True, blank=True)
    phone_regex = RegexValidator(
        regex=r"^\+?98?\d{10,10}$", message="شماره را به صورت +98123456789 وارد کنید"
    )
    phone_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)
    email = models.EmailField()
    avatar = models.CharField(max_length=255, null=True, blank=True)
    avatar_medium = models.CharField(max_length=255, null=True, blank=True)
    avatar_full = models.CharField(max_length=255, null=True, blank=True)

    # Add the other fields that can be retrieved from the Web-API if required

    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    class Meta:
        indexes = [
            models.Index(fields=['steam_id']),
        ]

    objects = SteamUserManager()

    def get_short_name(self):
        return self.person_name

    def get_full_name(self):
        return self.person_name

    def get_items(self, game_id):
        if self.pk is 1:
            return f"https://steamcommunity.com/inventory/76561198314354012/{game_id}/2?l=english&count=50"
        else:
            return f"https://steamcommunity.com/inventory/{self.steam_id}/{game_id}/2?l=english&count=5000"


class BankAccount(models.Model):
    bank_name = models.CharField(max_length=20)
    card_number = models.IntegerField()
    iban = models.IntegerField()
    owner = models.ForeignKey(SteamUser, on_delete=models.CASCADE)


class Wallet(models.Model):
    balance = models.DecimalField(
        max_digits=64, decimal_places=2, default=0, blank=True, null=True
    )
    owner = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    # objects = WalletManager()

    class Meta:
        verbose_name = _("wallet")
        verbose_name_plural = _("wallets")
        indexes = [
            models.Index(fields=['owner']),
        ]

    def __str__(self):
        return f"{self.owner} : {self.balance}"

    def get_absolute_url(self):
        return reverse("_detail", kwargs={"pk": self.pk})

    def deposit(self, amount, kind):
        self.balance += amount
        self.save()
        WalletTransaction.objects.create_deposit_transaction(self, amount, kind)

    def withdraw(self, amount, kind, bank=None):
        if self.withdraw_valid(amount):
            self.balance -= amount
            self.save()
            transaction = WalletTransaction.objects.create_withdraw_transaction(
                self, amount, kind, bank
            )
            return transaction
        raise Exception("not enough credit", )

    def withdraw_valid(self, amount):
        if amount > self.balance:
            return False
        return True


class WalletTransaction(models.Model):
    amount = models.DecimalField(
        max_digits=64, decimal_places=2, default=0
    )
    wallet = models.ForeignKey(Wallet, on_delete=models.SET_NULL, null=True, related_name="transactions")
    bank = models.ForeignKey(BankAccount, on_delete=models.SET_NULL, null=True)
    running_balance = models.DecimalField(
        max_digits=64, decimal_places=2, default=0
    )
    kind = models.CharField(max_length=20)
    created_at = models.DateTimeField(_("created_at"), auto_now_add=True)

    objects = WalletTransactionManager()

    class Meta:
        verbose_name = _("Wallet Transaction")
        verbose_name_plural = _("Wallet Transactions")
        indexes = [
            models.Index(fields=['created_at']),
        ]

    def __str__(self):
        return f"{self.amount} -> {self.wallet.owner}"

    def get_absolute_url(self):
        return reverse("WalletTransaction_detail", kwargs={"pk": self.pk})
