from django.contrib.auth import logout
from django.shortcuts import redirect, get_object_or_404
from django.views import View
from rest_framework import viewsets, mixins
from rest_framework.decorators import action
from rest_framework.response import Response

from Itemist.permissions import IsOwnerOrReadOnly, IsOwnerOrAdmin
from accounts.models import SteamUser, Wallet, WalletTransaction, BankAccount
from .serializers import (
    SteamUserSerializer,
    WalletSerializer,
    WalletTransactionSerializer,
    BankAccountSerializer)
from zarinpal.utils import start_transaction


class LoginView(View):
    def get(self, request):
        return redirect("social:begin", backend="steam")


class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect("accounts:login")


class ProfileViewSet(mixins.RetrieveModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin,
                     viewsets.GenericViewSet):
    """ endpoint to get user profile, update or delete if user itself request it."""
    serializer_class = SteamUserSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    queryset = SteamUser.objects.all()


class WalletViewSet(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    """endpoint to get or charge wallet if user itself is request it."""
    serializer_class = WalletSerializer
    permission_classes = (IsOwnerOrAdmin,)
    queryset = Wallet.objects.all()

    @action(detail=True, methods=["post"])
    def charge_account(self, request, pk=None):
        transaction_data = {
            "user": request.user,
            "first_name": request.user.first_name,
            "last_name": request.user.last_name,
            "amount": request.data.get("balance"),
            "callback_url": request.data.get("callback_url", None),
            "description": "charge account",
            "email": request.user.email,
            "mobile": request.user.phone_number,
        }
        return Response({"redirect_url": start_transaction(transaction_data)})

    @action(detail=True, methods=["POST"])
    def withdraw(self, request, pk):
        transaction = Wallet.objects.get(pk=pk).withdraw(
            amount=int(request.data["amount"]),
            kind="withdraw to user bank account",
            bank=BankAccount.objects.get(pk=request.data["bank"])
        )
        return Response(WalletTransactionSerializer(transaction).data)


class WalletTransactionViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet):
    """endpoint for listing all user transactions if user itself is requesting"""
    serializer_class = WalletTransactionSerializer
    permission_classes = (IsOwnerOrAdmin,)

    def get_queryset(self):
        return WalletTransaction.objects.filter(wallet__owner=self.request.user).all()


class BankAccountsViewSet(mixins.CreateModelMixin, mixins.ListModelMixin, mixins.DestroyModelMixin,
                          viewsets.GenericViewSet):
    permission_classes = (IsOwnerOrAdmin,)
    queryset = BankAccount.objects.all()
    serializer_class = BankAccountSerializer
