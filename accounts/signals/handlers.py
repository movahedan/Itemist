from django.conf import settings
from django.dispatch import receiver

from zarinpal.signals import transaction_verified


@receiver(transaction_verified)
def charge_account(sender, **kwargs):
    print("signal received")
    transaction = kwargs.get("transaction")
    if transaction.is_successful():
        charge_account_description = getattr(
            settings, "CHARGE_ACCOUNT_DESCRIPTION", "charge account"
        )
        if transaction.description == charge_account_description:
            steam_user_to_charge = transaction.user
            steam_user_to_charge.wallet.deposit(
                amount=transaction.amount, kind=transaction.description
            )
