from django.apps import apps
from django.contrib.auth.base_user import BaseUserManager
from django.db.models import manager

from loguru import logger


class SteamUserManager(BaseUserManager):
    def _create_user(self, steam_id, password, **extra_fields):
        """
        Creates and saves a User with the given steam_id and password.
        """
        try:
            # python social auth provides an empty email param, which cannot be used here
            del extra_fields["email"]
        except KeyError:
            pass
        if not steam_id:
            logger.log("ERROR", "cannot authorize user with no steam id")
            raise ValueError("The given steam_id must be set")
        user = self.model(steam_id=steam_id, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        Wallet = apps.get_model("accounts", "wallet")
        Wallet.objects.create(owner=user)

        logger.log("INFO", f"user registered successfully with steam id{user.steam_id}")
        return user

    def create_user(self, steam_id, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(steam_id, password, **extra_fields)

    def create_superuser(self, steam_id, password, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(steam_id, password, **extra_fields)


class WalletTransactionManager(manager.Manager):
    def create_deposit_transaction(self, wallet, amount, kind):
        return self.create(
            amount=amount,
            wallet=wallet,
            running_balance=amount + wallet.balance,
            kind=kind,
        )

    def create_withdraw_transaction(self, wallet, amount, kind, bank):
        return self.create(
            amount=amount,
            wallet=wallet,
            running_balance=wallet.balance - amount,
            kind=kind,
            bank=bank
        )
