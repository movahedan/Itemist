from django.contrib import admin
from .models import SteamUser, Wallet, WalletTransaction, BankAccount


@admin.register(SteamUser)
@admin.register(Wallet)
@admin.register(WalletTransaction)
@admin.register(BankAccount)
class SteamUserAdmin(admin.ModelAdmin):
    pass
