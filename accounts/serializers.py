from rest_framework import serializers

from .models import SteamUser, WalletTransaction, Wallet, BankAccount


class SteamUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = SteamUser
        fields = ("first_name", "last_name", "pk", "trade_url", "phone_number")
        read_only_fields = ("person_name", "avatar_medium", "avatar_full")


class WalletTransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = WalletTransaction
        fields = ("pk", "amount", "kind", "created_at")


class WalletSerializer(serializers.ModelSerializer):
    transactions = WalletTransactionSerializer(many=True)

    class Meta:
        model = Wallet
        fields = ("balance", "transactions")

    def get_callback_url(self, pk):
        pass


class BankAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = BankAccount
        exclude = ("owner",)

    def create(self, validated_data):
        return BankAccount.objects.create(
            **validated_data,
            owner=self.context["request"].user
        )
