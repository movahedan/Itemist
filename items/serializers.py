from rest_framework import serializers

from .models import Dota2Item, Item


class Dota2ItemsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Dota2Item
        fields = (
            "app_id",
            "context_id",
            "asset_id",
            "class_id",
            "instance_id",
            "name",
            "market_name",
            "price",
            "id",
            "name",
            "tradable",
            "marketable",
            "image_url_small",
            "image_url_large",
            "rarity_color",
            "name_color",
            "tradable",
            "quality",
            "hero",
            "item_type",
            "rarity",
            "slot",
        )


class ItemSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Item
        fields = (
            "id",
            "app_id",
            "context_id",
            "asset_id",
            "class_id",
            "instance_id",
            "name",
            "market_name",
            "price",
        )
