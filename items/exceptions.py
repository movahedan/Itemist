class UpdateItemsException(Exception):
    """ Exceptions during update inventory"""


class CouldNotFetchSteamInventory(UpdateItemsException):
    """ Failed to load steam inventory """
