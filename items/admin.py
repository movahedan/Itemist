from django.contrib import admin

from .models import Dota2Item

# Register your models here.

admin.site.register(Dota2Item)
