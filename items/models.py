from django.db import models

from polymorphic.models import PolymorphicModel

from accounts.models import SteamUser
from .managers import Dota2ItemManager


class Item(PolymorphicModel):
    app_id = models.IntegerField()
    context_id = models.IntegerField()
    asset_id = models.IntegerField()
    class_id = models.IntegerField()
    instance_id = models.IntegerField()
    name = models.CharField(max_length=100)
    market_name = models.CharField(max_length=300)
    price = models.IntegerField()
    image_relative_url_s = models.CharField(max_length=500, null=True, blank=True)
    image_relative_url_l = models.CharField(max_length=500, null=True, blank=True)
    name_color = models.CharField(max_length=100, null=True, blank=True)
    date_created = models.DateField(auto_now=True)
    quantity = models.IntegerField(default=1)
    who_added = models.ForeignKey(
        SteamUser, on_delete=models.CASCADE, related_name="added_by"
    )
    owner = models.ForeignKey(SteamUser, on_delete=models.CASCADE, related_name="items")
    tradable = models.BooleanField()
    marketable = models.BooleanField()

    def image_url_large(self):
        return (
            "https://steamcommunity-a.akamaihd.net/economy/image/"
            + self.image_relative_url_l
        )

    def image_url_small(self):
        return (
            "https://steamcommunity-a.akamaihd.net/economy/image/"
            + self.image_relative_url_s
        )

    class Meta:
        # abstract = True
        ordering = ["date_created"]


class Dota2Item(Item):
    QUALITY_CHOICES = [
        ("Auspicious", "Auspicious"),
        ("Autographed", "Autographed"),
        ("Base", "Base"),
        ("Genuine", "Genuine"),
        ("Inscribed", "Inscribed"),
        ("Standard", "Standard"),
    ]
    RARITY_CHOICES = [
        ("Common", "Common"),
        ("Immortal", "Immortal"),
        ("Legendary", "Legendary"),
        ("Mythical", "Mythical"),
        ("Rare", "Rare"),
        ("Seasonal", "Seasonal"),
        ("Uncommon", "Uncommon"),
    ]
    TYPE_CHOICES = [
        ("Announcer", "Announcer"),
        ("Bundle", "Bundle"),
        ("Courier", "Courier"),
        ("CursorPack", "CursorPack"),
        ("DireCreeps", "DireCreeps"),
        ("HUDSkin", "HUDSkin"),
        ("LoadingScreen", "LoadingScreen"),
        ("Misc", "Misc"),
        ("Music", "Music"),
        ("RadiantCreeps", "RadiantCreeps"),
        ("Taunt", "Taunt"),
        ("Terrain", "Terrain"),
        ("Tool", "Tool"),
        ("Ward", "Ward"),
        ("Wearable", "Wearable"),
    ]
    #   fields
    quality = models.CharField(
        max_length=15, choices=QUALITY_CHOICES, default="Auspicious"
    )
    hero = models.CharField(max_length=30)
    item_type = models.CharField(max_length=20, choices=TYPE_CHOICES)
    rarity = models.CharField(max_length=10, choices=RARITY_CHOICES)
    slot = models.CharField(max_length=300)
    objects = Dota2ItemManager()

    def quality_color(self):
        return {
            "standard": "rgb(210, 210, 210)",
            "inscribed": "rgb(207, 106, 50)",
            "auspicious": "rgb(50, 205, 50)",
            "heroic": "rgb(134, 80, 172)",
            "genuine": "rgb(77, 116, 85)",
            "autographed": "rgb(173, 229, 92)",
            "frozen": "rgb(70, 130, 180)",
            "cursed": "rgb(134, 80, 172)",
            "base": "rgb(178, 178, 178)",
            "corrupted": "rgb(136, 71, 255)",
            "infused": "rgb(136, 71, 255)",
            "unusual": "rgb(134, 80, 172)",
            "exalted": "rgb(204, 204, 204)",
            "elder": "rgb(71, 98, 145)",
            "legacy": "rgb(255, 255, 255)",
        }.get(self.quality.lower(), "")

    def rarity_color(self):
        return {
            "common": "rgb(176, 195, 217)",
            "uncommon": "rgb(94, 152, 217)",
            "rare": "rgb(75, 105, 255)",
            "mythical": "rgb(136, 71, 255)",
            "immortal": "rgb(228, 174, 57)",
            "legendary": "rgb(211, 44, 230)",
            "arcana": "rgb(173, 229, 92)",
            "ancient": "rgb(235, 75, 75)",
        }.get(self.rarity.lower(), "")

    def name_color(self):
        pass

    def type_color(self):
        pass

    def slot_color(self):
        pass

    def hero_color(self):
        pass
