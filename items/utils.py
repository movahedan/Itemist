import sys
import requests
import random

from loguru import logger

from items.models import Dota2Item
from .config import GAME_IDS
from .exceptions import CouldNotFetchSteamInventory

logger.add(
    sys.stderr, format="{time} {level} {message}", filter="trade_offers", level="INFO"
)


def update_user_items_dota2(steam_user):
    items = get_user_items(steam_user, "dota2")
    for item_description in get_items_description(items):
        item_info = get_item_info(
            get_class_id(item_description), get_instance_id(item_description), items
        )
        Dota2Item.objects.create_dota2_item_from_steam_api(
            steam_user, item_description, item_info
        )


def get_user_items(steam_user, app_name):
    # Send a request to the API server and store the response.
    response = requests.get(
        steam_user.get_items(GAME_IDS[app_name]), headers=get_random_header()
    )
    if response.text == "null":
        logger.error(
            f"could not fetch inventory of user {steam_user.steam_id} in game: {app_name} with url:\
            {steam_user.get_items(GAME_IDS[app_name])}",
        )
        raise CouldNotFetchSteamInventory("Steam returned no inventory")
    else:
        logger.info(
            f"updated inventory of user {steam_user.steam_id} for game: {app_name}"
        )
    return response.json()


def get_random_header():
    headers = (
        {
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36"
        },
        {},
    )
    return headers[random.randint(0, len(headers) - 1)]


def get_items_description(items):
    return items["descriptions"]


def get_item_info(class_id, instance_id, items):
    for item_info in items["assets"]:
        if item_info["classid"] == class_id and item_info["instanceid"] == instance_id:
            return item_info
    return None


def get_instance_id(item_description):
    try:
        return item_description["instanceid"]
    except KeyError:
        logger.error(f"data is invalid {item_description}, no instanceid")
        Exception("cannot find necessary data instanceid in item_description")


def get_class_id(item_description):
    try:
        return item_description["classid"]
    except KeyError:
        logger.erorr(f"data is invalid {item_description}, no classid")
        Exception("cannot find necessary data classid in item_description")


def get_game_id(game, game_ids):
    try:
        return game_ids[game]
    except KeyError:
        logger.error(f"game: {game} is not defined")
        Exception(f"cannot find game: {game}")
