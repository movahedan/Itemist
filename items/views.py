from rest_framework import viewsets
from rest_framework.exceptions import NotFound

from Itemist.permissions import IsOwnerOrReadOnly
from .models import Item
from .utils import update_user_items_dota2, get_game_id
from .serializers import Dota2ItemsSerializer, ItemSerializer
from .config import GAME_IDS


class InventoryViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows users to view and update their inventory.
    """

    def get_serializer_class(self):
        return self.serializer_switch()

    def get_queryset(self):
        app_id = get_game_id(self.get_game_parameter(), GAME_IDS)
        if app_id is not "unidentified":
            update_user_items(request=self.request, game=self.get_game_parameter())
            self.queryset = Item.objects.filter(owner=self.request.user, app_id=app_id)
        else:
            self.queryset = None
        return self.queryset

    def serializer_switch(self):
        try:
            return {"dota2": Dota2ItemsSerializer}[self.get_game_parameter()]
        except KeyError:
            raise NotFound(detail="game is not correct", code=404)

    def get_game_parameter(self):
        return self.request.query_params.get("game", None)


class ItemViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allow users to get item details like ids
    """
    serializer_class = ItemSerializer
    permission_classes = (IsOwnerOrReadOnly,)

    def get_queryset(self):
        pk = self.request.query_params.get("pk", None)
        self.queryset = Item.objects.filter(owner=self.request.user, pk=pk)
        return self.queryset


def update_user_items(request, game):
    if game == "dota2":
        update_user_items_dota2(request.user)
    else:
        print(game)
