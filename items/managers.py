from django.apps import apps

from polymorphic.managers import PolymorphicManager


class Dota2ItemManager(PolymorphicManager):
    def create_dota2_item_from_steam_api(self, steam_user, item_description, item_info):
        # Dota2Item = apps.get_model('items', 'Dota2Item')
        dota2_item, created = self.get_or_create(
            app_id=item_description.get("appid"),
            context_id=item_info.get("contextid"),
            asset_id=item_info.get("assetid"),
            class_id=item_info.get("classid"),
            instance_id=item_info.get("instanceid"),
            name=item_description.get("name"),
            market_name=item_description.get("market_name"),
            price=0,
            image_relative_url_s=item_description.get("icon_url"),
            image_relative_url_l=item_description.get("icon_url_large"),
            name_color=item_description.get("name_color"),
            who_added=steam_user,
            owner=steam_user,
            tradable=item_description.get("tradable"),
            marketable=item_description.get("marketable"),
            quality=item_description.get("tags")[0].get("localized_tag_name"),
            rarity=item_description.get("tags")[1].get("localized_tag_name"),
            item_type=item_description.get("tags")[2].get("localized_tag_name"),
            slot=item_description.get("tags")[3].get("localized_tag_name"),
            hero=item_description.get("tags")[4].get("localized_tag_name"),
        )
        if created:
            return dota2_item
        else:
            dota2_item.quantity += 1
