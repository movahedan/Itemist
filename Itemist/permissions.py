from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.id == request.user.id


class IsOwnerOrAdmin(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        if hasattr(request, "user"):
            return request.user.is_superuser
        if hasattr(obj, "owner"):
            return obj.owner == request.user
        elif hasattr(obj, "wallet"):
            return obj.wallet.owner == request.user
        elif hasattr(obj, "seller"):
            return obj.seller == request.user
        elif hasattr(obj, "sender") or hasattr(obj, "receiver"):
            return obj.sender == request.user or obj.receiver == request.user
