from rest_framework import routers

from items import views as item_views

router = routers.DefaultRouter()
router.register(r"items/inventory", item_views.InventoryViewSet, base_name="inventory")
router.register(r"items/details", item_views.ItemViewSet, base_name="details")
