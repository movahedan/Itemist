## **What is Itemist**
Itemist is an game item trading platform that allows users to buy or sell or trade steam items in this app.
Itemist is targeting Iranian users since sell and buy is not a straight forward process for them.
this project is going to be a simple, beautiful and painless platform for iranian users who want to sell/buy/trade steam items.

## **How to Start Development**
If you want to know more about front-end client app visit https://github.com/Glyphack/Itemist-Client
* ### 1 - getting the code

after cloning the project you need to update submodules:
```
git submodule init
git submodule update
```
* ### Step2 - virtual environment
activate pipenv environment.
```
pipenv shell
```

* ### Step3 - Project Config
create a secret_settings.py file in Itemist/ along with the settings.py and fill it with these data:

`SECRET_KEY,
 API_KEY,
 TEST_ADMIN_PASSWORD,
 TEST_ADMIN_STEAM_ID,
 API_KEY
STEAM_USERNAME
STEAM_PASSWORD
PATH_TO_STEAM_GUARD_FILE` 

these are all strings.

after this run commands:
```
./manage.py migrate
./manage.py runserver
```
now you should be able to visit app at localhost and you can login at localhost/admin with
```
steam_id=what you setted in TEST_ADMIN_STEAM_ID
password=what you setted in TEST_ADMIN_PASSWORD
```
after login you are able to use /admin and send request to api and get test results without any need to have an steam account with items

## **Task Management**
We use trello for task management at the address:
https://trello.com/b/OLCo9CPc/itemist

## **API**
we use several apps for website features I list them and then list their endpoints.


### accounts
`profile`
GET: retrieve user profile.

PUT: updates user profile. **only for user itself**

DELETE: deletes user profile. **only for user itself**


`wallet`

GET: show user's balance. **only for user itself**

usage:
```rest
wallet/<SteamUserId>
```

actions: `charge account` requires the amount and returns a url to redirect user to zarinpal. **only for user itself**

usage:
```rest
wallet/<SteamUserId>/charge_account
```
### items
`items/inventory?game=<game-name>`
returns specified game inventory of user

`items/details` returns detail about item you are going to sell.

### shop

Get: returns items listed for selling with filter parameters

filters: 

user: name of user & sold: boolean

you can retrieve a sell order information using its pk.

Post: 

you can create sell order for user using 

`item_id`: item id int
`price`: in tomans float

owner of an Item can edit the price of the item or delete it.


Post: you can create buy orders buy giving sell order ids, 
the server will verify user has sufficient amount of money to
buy items. after that application creates buy orders for every single item
in the sell order list in the parameter and after all it will send a 
trade offer to the user and returns the trade offer as json response.

`sell_order_pks`: list of sell order pks user wants to buy
